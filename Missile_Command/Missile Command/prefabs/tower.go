components {
  id: "script"
  component: "/scripts/tower.script"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "collisionobject"
  type: "collisionobject"
  data: "collision_shape: \"\"\n"
  "type: COLLISION_OBJECT_TYPE_TRIGGER\n"
  "mass: 0.0\n"
  "friction: 0.1\n"
  "restitution: 0.5\n"
  "group: \"default\"\n"
  "mask: \"default\"\n"
  "embedded_collision_shape {\n"
  "  shapes {\n"
  "    shape_type: TYPE_BOX\n"
  "    position {\n"
  "      x: 17.0\n"
  "      y: 4.0\n"
  "      z: 0.0\n"
  "    }\n"
  "    rotation {\n"
  "      x: 0.056441195\n"
  "      y: 0.023027672\n"
  "      z: -0.3361925\n"
  "      w: 0.93981844\n"
  "    }\n"
  "    index: 0\n"
  "    count: 3\n"
  "  }\n"
  "  shapes {\n"
  "    shape_type: TYPE_BOX\n"
  "    position {\n"
  "      x: -18.0\n"
  "      y: 3.0\n"
  "      z: 0.0\n"
  "    }\n"
  "    rotation {\n"
  "      x: 0.09166168\n"
  "      y: 0.076309815\n"
  "      z: 0.34480315\n"
  "      w: 0.931067\n"
  "    }\n"
  "    index: 3\n"
  "    count: 3\n"
  "  }\n"
  "  shapes {\n"
  "    shape_type: TYPE_SPHERE\n"
  "    position {\n"
  "      x: 0.0\n"
  "      y: -16.0\n"
  "      z: 0.0\n"
  "    }\n"
  "    rotation {\n"
  "      x: 0.0\n"
  "      y: 0.0\n"
  "      z: 0.0\n"
  "      w: 1.0\n"
  "    }\n"
  "    index: 6\n"
  "    count: 1\n"
  "  }\n"
  "  data: 7.5\n"
  "  data: 25.0\n"
  "  data: 0.5\n"
  "  data: 7.5\n"
  "  data: 25.0\n"
  "  data: 0.5\n"
  "  data: 15.0\n"
  "}\n"
  "linear_damping: 0.0\n"
  "angular_damping: 0.0\n"
  "locked_rotation: false\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "Tower_Sprite"
  type: "sprite"
  data: "tile_set: \"/assets/images/Comp.atlas\"\n"
  "default_animation: \"tower\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "wall_left"
  type: "sprite"
  data: "tile_set: \"/assets/images/Comp.atlas\"\n"
  "default_animation: \"wall\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 35.0
    y: -13.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.42261827
    w: 0.9063078
  }
}
embedded_components {
  id: "wall_right"
  type: "sprite"
  data: "tile_set: \"/assets/images/Comp.atlas\"\n"
  "default_animation: \"wall\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -37.0
    y: -13.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: -0.42261827
    w: 0.9063078
  }
}
