#!/bin/bash
WIN="0"
PLAYER="O"
MAP=(" " " " " " " " " " " " " " " " " ")
CURSOR=4

function printMap {
	clear
	echo "Use wasd to change cursor position and enter to make mark."
	TEXT=""
	echo "╔═╤═╤═╗"
	for I in {0..2}
	do
		TEXT+="║"
		for J in {0..2}
		do
			if (( $(($I*3+$J)) == $CURSOR)) 
			then
				TEXT+="\e[100m${MAP[$(($I*3+$J))]}\e[49m"
			else 
				TEXT+="${MAP[(($I*3+$J))]}"
			fi
		if [ "$J" = "2" ] 
		then
			TEXT+="║"
		else 
			TEXT+="│"
		fi
		done
	echo -e $TEXT
	TEXT=""
		if [ "$I" = "2" ] 
		then
			echo "╚═╧═╧═╝"
		else 
			echo "╟─┼─┼─╢"
		fi
	done
	

}

function win {
	printMap
        echo "Player ${PLAYER} has won!!"
	
        WIN="1"

}

function stalmate {
	printMap
	echo "Stalmate! Neither player has won!"
        WIN="1"
}

function checkStelmate {
	for POS in "${MAP[@]}"
	do 
		if [ "$POS" = " " ] 
		then
			return 1
		fi
	done
	stalmate
	return 0
}

function checkWin {
	for POLE in {0..2}
	do
		ROW=POLE*3

		#rows
		if [ "${MAP[$ROW]}" = "$PLAYER" ] && [ "${MAP[$ROW+1]}" = "$PLAYER" ] && [ "${MAP[$ROW+2]}" = "$PLAYER" ]
		then
			win
			return 0
		fi
		ROW=POLE
                #columns
                if [ "${MAP[$ROW]}" = "$PLAYER" ] && [ "${MAP[$ROW+3]}" = "$PLAYER" ] && [ "${MAP[$ROW+6]}" = "$PLAYER" ]
                then    
                        win
			return 0
                fi
	done

	#cross
	if [ "${MAP[0]}" = "$PLAYER" ] && [ "${MAP[4]}" = "$PLAYER" ] && [ "${MAP[8]}" = "$PLAYER" ]
	then
	       	win
		return 0
	fi
	if [ "${MAP[2]}" = "$PLAYER" ] && [ "${MAP[4]}" = "$PLAYER" ] && [ "${MAP[6]}" = "$PLAYER" ]
        then
                win
		return 0
        fi
	checkStelmate
	return 0
}

function nextTurn {
	if [ "$PLAYER" = "O" ]
	then
		PLAYER="X"
	else 
		PLAYER="O"
	fi
}

function markPos {
	if [ "${MAP[$CURSOR]}" = " " ]
	then
		MAP[$CURSOR]="$PLAYER"
		return 1
	fi
	return 0
}

function changePosition {

		CURSOR=$(($CURSOR+$1))
		CURSOR=$(($CURSOR+3*$2))
		if ((${CURSOR} > 8)) 
		then
			CURSOR=0
		fi
		if ((0 > ${CURSOR}))
		then
			CURSOR=8
		fi
		printMap
	
}

function getInput {
	RESULT=0
	while [ "$RESULT" = "0" ]
	do
	read -s -n 1 KEY
	case "$KEY" in
		"w") changePosition 0 -1 ;;
		"s") changePosition 0 1 ;;
		"a") changePosition -1 0 ;; 
		"d") changePosition 1 0 ;;
		"") markPos; RESULT=$?  ;;
	esac
	done
	

}


function loop {
	while [ ${WIN} -eq "0" ]
	do
	printMap
	getInput
	checkWin
	nextTurn
	done
}

loop


