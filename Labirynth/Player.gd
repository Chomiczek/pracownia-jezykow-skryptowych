extends RigidBody2D
var animation_machine
export (int) var speed = 2000
var lives = 3

# Called when the node enters the scene tree for the first time.
func _ready():
	contact_monitor = true
	contacts_reported = 10
	set_mode(RigidBody2D.MODE_CHARACTER)
	animation_machine = $AnimationTree.get("parameters/playback")


func get_input():
	
	var velocity = Vector2()
	if(Input.is_action_pressed("Up")): velocity.y -=1
	if(Input.is_action_pressed("Down")): velocity.y +=1
	if(Input.is_action_pressed("Right")): velocity.x +=1
	if(Input.is_action_pressed("Left")): velocity.x -=1
	return velocity.normalized()*speed

func animateMove(velocity):
	
	if(abs(velocity.y)>abs(velocity.x)):
		if(velocity.y>0): animation_machine.travel("walk_down")
		elif(velocity.y<0): animation_machine.travel("walk_up")
		else: animation_machine.travel("Idle")	
	else:
		if(velocity.x>0): animation_machine.travel("walk_right")
		elif(velocity.x<0): animation_machine.travel("walk_left")
		else: animation_machine.travel("Idle")	
		

func move(velocity):
	animateMove(velocity)
	if(velocity == Vector2()): pass
	else:
		apply_central_impulse(velocity)

func checkCollision():
	var list = get_colliding_bodies()
	for o in list:
		if("Enemy" in o.name):
			die()

func die():
	lives -=1
	if(lives <=0): get_tree().reload_current_scene()
	#print("Lives: " + str(lives))
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	move(get_input()*delta)
	checkCollision()
