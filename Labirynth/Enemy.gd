extends RigidBody2D
var animation_machine
export (int) var speed = 1700
export (int) var fov = 3*64

var rng = RandomNumberGenerator.new()
var random_velocity
var moves = 0
export (int) var moves_reload = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	contact_monitor = true
	contacts_reported = 10
	set_mode(RigidBody2D.MODE_CHARACTER)
	animation_machine = $AnimationTree.get("parameters/playback")
	rng.randomize()

func ai():
	var player_pos = get_parent().get_parent().get_node("Player").position
	if(fov<self.position.distance_to(player_pos)): 
		moves=moves-1
		if(moves <= 0):
			moves = moves_reload
			random_velocity =  Vector2(rng.randf_range(-2,2),rng.randf_range(-2,2)).normalized()*(speed/2)
		return random_velocity
	else:
		return self.position.direction_to(player_pos).normalized()*speed
	

func animateMove(velocity):	
	if(abs(velocity.y)>abs(velocity.x)):
		if(velocity.y>0): animation_machine.travel("walk_down")
		elif(velocity.y<0): animation_machine.travel("walk_up")
		else: animation_machine.travel("Idle")	
	else:
		if(velocity.x>0): animation_machine.travel("walk_right")
		elif(velocity.x<0): animation_machine.travel("walk_left")
		else: animation_machine.travel("Idle")	

func move(velocity):
	animateMove(velocity)
	if(velocity == Vector2()): pass
	else:
		apply_central_impulse(velocity)
	
func checkCollision():
	var list = get_colliding_bodies()
	for o in list:
		if("Player" in o.name):
			get_parent().remove_child(self)

func _physics_process(delta):
	move(ai()*delta)
	checkCollision()
	
