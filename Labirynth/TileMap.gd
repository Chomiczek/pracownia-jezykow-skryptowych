extends TileMap

var size = Vector2(4,4)
export (Vector2) var old_size = Vector2(4,4)
export (int) var enemiesNumber = 1


var stairs_pos

var rng = RandomNumberGenerator.new()

var map = []

func getNeighbours(current):
	var nb = []
	var temp = Vector2()
	
	temp = current + Vector2(0,1)
	if(temp.x>=0 and temp.y>=0 and temp.x<size.x and temp.y<size.y):
		if(map[temp.x][temp.y]==Vector2(-1,-1)): 
			nb.append(temp)
	
	temp = current + Vector2(0,-1)
	if(temp.x>=0 and temp.y>=0 and temp.x<size.x and temp.y<size.y):
		if(map[temp.x][temp.y]==Vector2(-1,-1)):
			nb.append(temp)
		
	temp = current + Vector2(1,0)
	if(temp.x>=0 and temp.y>=0 and temp.x<size.x and temp.y<size.y):
		if(map[temp.x][temp.y]==Vector2(-1,-1)): 
			nb.append(temp)
		
	temp = current + Vector2(-1,0)
	if(temp.x>=0 and temp.y>=0 and temp.x<size.x and temp.y<size.y):
		if(map[temp.x][temp.y]==Vector2(-1,-1)):
			nb.append(temp)
			
	return nb
	
func generateMaze():
	map = []
	randomize()
	for i in range(size.y+1):
		map.append([])
		for j in range(size.x+1):
			map[i].append(Vector2(-1,-1))
	
	var pos = Vector2(0,0)
	var path_stack = []
	var nb_list = []
	path_stack.push_back(pos)
	
	while(path_stack.size()!= 0 ):
		pos = path_stack.pop_back()
		nb_list = getNeighbours(pos)
		
		if(nb_list.size()>0):
			nb_list.shuffle()
			for nb in nb_list:
				map[nb.x][nb.y] = pos
				path_stack.push_back(nb)
	
	fillMaze()
	rng.randomize()
	var imperfect = size.x*size.y*rng.randf_range(0.05,0.25)
	
	for i in range(imperfect):
		pos = Vector2(rng.randi_range(0,size.x),rng.randi_range(0,size.y))
		while(map[pos.x][pos.y] == Vector2(-1,-1)):
			pos = Vector2(rng.randi_range(0,size.x),rng.randi_range(0,size.y))
		map[pos.x][pos.y] = Vector2(-1,-1)
		
func fillMaze():
	size.x *=2
	size.y *=2
	var new_map = []
	for i in range(size.y+1):
		new_map.append([])
		for j in range(size.x+1):
			new_map[i].append(Vector2(-1,-1))
			if(i%2==0 and j%2==0): new_map[i][j] = map[i/2][j/2]*2
			
	var temp = Vector2()
	
	for i in range(size.y+1):
		for j in range(size.x+1):
			if(new_map[i][j] == Vector2(-1,-1)): continue
			temp = (new_map[i][j] - Vector2(i,j)).normalized()
			temp = temp + Vector2(i,j)
			new_map[temp.x][temp.y] = new_map[i][j]
			
			
	
	
	map = new_map
	
func setOuterWall(twall, tfloor):
	for i in range(0,size.x):
		if(get_cell(i,0) != twall): set_cell(i,0,twall)
		if(get_cell(i,1) == twall): set_cell(i,1,tfloor)
		
		if(get_cell(i,size.y) != twall): set_cell(i,size.y,twall)
		if(get_cell(i,size.y-1) == twall): set_cell(i,size.y-1,tfloor)
		
		if(get_cell(0,i) != twall): set_cell(0,i,twall)
		if(get_cell(1,i) == twall): set_cell(1,i,tfloor)
		
		if(get_cell(size.x,i) != twall): set_cell(size.x,i,twall)
		if(get_cell(size.x-1,i) == twall): set_cell(size.x-1,i,tfloor)
	set_cell(size.x,size.y,twall)
	
func getFloors(tfloor):
	
	var floor_list = []
	for i in range(size.y+1):
		for j in range(size.x+1):
			if(get_cell(i,j) == tfloor):
				floor_list.append(Vector2(i,j))
	
	return floor_list
				
func constructMaze():
	
	var twall = tile_set.find_tile_by_name("walls")
	var tfloor = tile_set.find_tile_by_name("floor")
	var tstairs =  tile_set.find_tile_by_name("stairs")
	generateMaze()
	
	for i in range(size.y+1):
		for j in range(size.x+1):
			if(map[i][j] == Vector2(-1,-1)): set_cell(j,i,tfloor)
			else: set_cell(j,i,twall)
	
	setOuterWall(twall,tfloor)
	
	var floor_list = getFloors(tfloor)
	rng.randomize()
	
	# set stairs position
	var num = rng.randi_range(0,floor_list.size()-1)
	stairs_pos = floor_list[num]
	set_cell(stairs_pos.x,stairs_pos.y,tstairs)
	floor_list.remove(num)
	
	var player_pos = setPlayerPos(floor_list,stairs_pos)

	setEnemies(floor_list,player_pos)
	
	update_bitmask_region()
		
func setEnemies(floor_list,player_pos):
	# set enemies position
	var num
	var enemie_fov =  7
	var enemie
	var node = get_parent().get_node("Enemies")
	for e in range(enemiesNumber):
		
		num = rng.randi_range(0,floor_list.size()-1)
		while(player_pos.distance_to(floor_list[num])<enemie_fov):
			num = rng.randi_range(0,floor_list.size()-1)
		enemie = load("res://Enemy.tscn").instance()
		node.add_child(enemie)
		enemie.position = floor_list[num]*64+ Vector2(32,32)
		floor_list.remove(num)

func setPlayerPos(floor_list,stairs_pos):
	# set player position
	var num = rng.randi_range(0,floor_list.size()-1)
	
	while(floor_list[num].distance_to(stairs_pos)<0.6*size.x):
		num = rng.randi_range(0,floor_list.size()-1)
	get_parent().get_node("Player").position = floor_list[num]*64 + Vector2(32,32)
	var player_pos = floor_list[num]
	
	floor_list.remove(num)
	
	return player_pos
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	newLevel()
	
func _process(delta):
	checkPlayer()
	
	
func checkPlayer():
	if(stairs_pos.distance_to(get_parent().get_node("Player").position)<30):
		newLevel()


func newLevel():
	
	old_size.x+=1
	old_size.y+=1
	size = old_size
	enemiesNumber = old_size.x-3
	constructMaze()
	
	stairs_pos = stairs_pos*64
	stairs_pos.x+=32
	stairs_pos.y+=32
	
	print(str(stairs_pos))
	print("size: " + str(size))
