import pygame

class Laser(object):

    def __init__(self, screen, position):
        self._screen = screen
        self.toKill = False

        self._sprite = pygame.image.load("sprites/laser.png")
        self._position = position
        self._scale = pygame.math.Vector2(1, 1)
        self.scale(pygame.math.Vector2(0.2, 0.2) )
        self._speed = -15

    def scale(self, scale):
        width = int(self._sprite.get_width() * (scale.x / self._scale.x))
        height = int(self._sprite.get_height() * (scale.y / self._scale.y))
        self._sprite = pygame.transform.scale(self._sprite, (width, height))
        self._scale = scale

    def setEnemyFire(self):
        self._speed = - self._speed

    def update(self):
        self.goVertical(self._speed)
        self._screen.draw(self._sprite, self._position)

    def getPosition(self):
        return self._position

    def GetSize(self):
        return pygame.math.Vector2(self._sprite.get_width(), self._sprite.get_height())

    def setPosition(self, position):
        self._position = position

    def goVertical(self, speed):
        self._position.y += speed
        if self._position.y < 0 or self._position.y > self._screen.getHeight():
            self.toKill = True
