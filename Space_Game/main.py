import pygame

import player as pl
import enemy as en
import laser
import utility

#  Bool value that says if the game is running or not
isRunning = False
start = False

main_screen = None
player = None
enemyGroup = None
laserList = []

# True if using mause controls
usingMouse = False

reloadTime = 5
reload = reloadTime

enemyReloadTime = 30
enemyReload = enemyReloadTime

def init():
    global main_screen, player, enemyGroup
    pygame.init()

    main_screen = utility.Screen()
    player = pl.Player(main_screen)
    player.scale(pygame.math.Vector2(2, 2))
    #pygame.event.set_grab(True)
    pygame.mouse.set_visible(False)

    enemyGroup = en.EnemyCluster(main_screen)
    player.scale(pygame.math.Vector2(2, 2))

def update():
    global isRunning, player,\
        enemyGroup, laserList, reload, \
        enemyReload, playerKilled, start

    clock = pygame.time.Clock()
    isRunning = True
    # main loop
    while isRunning:
        if not start:
            main_screen.update()
            main_screen.startGame()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    start = True

        elif not player.isAlive:
            main_screen.update()
            main_screen.endGame()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    isRunning = False

        else:
            if not (reload == 0):
                reload -= 1
            if not (enemyReload == 0):
                enemyReload -= 1
            checkEvents()
            main_screen.update()
            player.checkHits(laserList)
            enemyGroup.checkHits(laserList)
            addEnemyLaser()
            updateLasers()
            player.update()
            enemyGroup.update()
            main_screen.guiUpdate()
        pygame.display.update()
        clock.tick(30)

def checkEvents():
    global isRunning, player, enemyGroup, usingMouse
    for event in pygame.event.get():
        # Game exit
        if event.type == pygame.QUIT or \
                (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            isRunning = False
        if usingMouse:
            if event.type == pygame.MOUSEMOTION:
                player.moveToCursor(pygame.mouse.get_pos())

            if pygame.mouse.get_pressed()[0]:
                addLaser(player.getGunPos())

    if not usingMouse:
        key_pressed = pygame.key.get_pressed()
        if key_pressed[pygame.K_a] or key_pressed[pygame.K_LEFT]:
            player.goLeft()
        elif key_pressed[pygame.K_d] or key_pressed[pygame.K_RIGHT]:
            player.goRight()
        if key_pressed[pygame.K_SPACE]:
            addLaser(player.getGunPos())


def addEnemyLaser():
    global laserList, main_screen, enemyReloadTime, enemyReload
    if not (enemyReload == 0):
        return
    else:
        position = enemyGroup.getRandomGun()
        las = laser.Laser(main_screen, position)
        las.setEnemyFire()
        laserList.append(las)
        enemyReload = enemyReloadTime

def addLaser(position):
    global laserList, main_screen, reload, reloadTime
    if not (reload == 0):
        return
    else:
        las = laser.Laser(main_screen, position)
        lasSize = las.GetSize()
        lasSize = pygame.math.Vector2(lasSize.x/2,lasSize.y)
        las.setPosition(position - lasSize)
        laserList.append(las)
        reload = reloadTime
        utility.score-=1


def updateLasers():
    global laserList

    for l in laserList:
        l.update()
        if l.toKill:
            laserList.remove(l)


def main():
    init()
    update()


main()
