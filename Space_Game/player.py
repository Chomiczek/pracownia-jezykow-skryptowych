import pygame

class Player(object):

    def __init__(self, screen):
        self._screen = screen
        # Player sprite
        self._sprite = pygame.image.load("sprites/player.png")
        # Player position
        self._position = self._screen.getCenter()
        self._position.y = self._screen.getHeight() - 2* self._sprite.get_height() - 10
        # Player scale
        self._scale = pygame.math.Vector2(1, 1)

        self.isAlive = True

        self._speed = 20

    def scale(self, scale):
        width = int(self._sprite.get_width()*(scale.x/self._scale.x))
        height = int(self._sprite.get_height()*(scale.y/self._scale.y))
        self._sprite = pygame.transform.scale(self._sprite, (width, height))
        self._scale = scale

    def getGunPos(self):
        return  self._position + pygame.math.Vector2(self._sprite.get_width()/2,0)

    def update(self):
        self._screen.draw(self._sprite, self._position)

    def goRight(self):
        self._position.x += self._speed
        if self._position.x > self._screen.getWidth()-self._sprite.get_width():
            self._position.x = self._screen.getWidth() - self._sprite.get_width()

    def goLeft(self):
        self._position.x -= self._speed
        if self._position.x < 0:
            self._position.x = 0

    def moveToCursor(self, cursor):
        direction = cursor - self._position
        if direction.x<0:
            self.goLeft()
        elif direction.x>0:
            self.goRight()

    def checkHits(self, laserList):
        playerSize = pygame.math.Vector2(self._sprite.get_width(), self._sprite.get_height())
        laserPos = None
        for laser in laserList:
            laserPos = laser.getPosition()
            dist = laserPos.distance_to(self._position + playerSize/2)
            if dist < playerSize.x and dist < playerSize.y:
                self.isAlive = False
                break


