import pygame

score = 0

class Screen(object):
    screen = None
    _scoreLabel = None
    _endLabel = None



    def __init__(self):
        self.setScreen()

    def setScreen(self):
        self.screen = pygame.display.set_mode((800, 600))
        pygame.display.set_caption("Space Game")
        pygame.display.set_icon(pygame.image.load("sprites/icon.png"))
        pygame.font.init()
        self._scoreLabel = pygame.font.SysFont('Comic Sans MS', 14)
        self._endLabel = pygame.font.SysFont('Comic Sans MS', 40)

    # Returns center of the screen as Vector2
    def getCenter(self):
        return pygame.math.Vector2(self.screen.get_width() / 2, self.screen.get_height() / 2)

    def getWidth(self):
        return self.screen.get_width()

    def getHeight(self):
        return self.screen.get_height()

    # Draws given sprite in given position on the screen
    def draw(self, sprite, position):
        self.screen.blit(sprite, position)
    def startGame(self):
        textsurface = self._endLabel.render("Press any key to start . . .", False, (255, 0, 0))
        self.screen.blit(textsurface,
                         (self.screen.get_width() / 2 - textsurface.get_width() / 2,
                          self.screen.get_height() / 2 - textsurface.get_height()))
        self.helpText(True)
    def endGame(self):
        textsurface = self._endLabel.render("Game over!", False, (255, 0, 0))
        self.screen.blit(textsurface,
                         (self.screen.get_width()/2 - textsurface.get_width()/2,
                          self.screen.get_height()/2 - textsurface.get_height()))
        textsurface = self._endLabel.render("Your score: " + str(score), False, (255, 0, 0))
        self.screen.blit(textsurface,
                         (self.screen.get_width() / 2 - textsurface.get_width() / 2,
                          self.screen.get_height()/2 + textsurface.get_height()/2))
    def helpText(self, on = False):
        if pygame.key.get_pressed()[pygame.K_h] or on:
            textsurface = self._scoreLabel.render("Use arrows to move.", False, (255, 255, 255))
            self.screen.blit(textsurface, (self.getWidth() - textsurface.get_width() - 10, 0))
            textsurface = self._scoreLabel.render("Use space to shoot.", False, (255, 255, 255))
            self.screen.blit(textsurface,
                             (self.getWidth() - textsurface.get_width() - 10, textsurface.get_height() + 2))
            textsurface = self._scoreLabel.render("Each shot cost 1 point.", False, (255, 255, 255))
            self.screen.blit(textsurface,
                             (self.getWidth() - textsurface.get_width() - 10, 2*(textsurface.get_height() + 2)))
            textsurface = self._scoreLabel.render("Each enemy grants 2 points.", False, (255, 255, 255))
            self.screen.blit(textsurface,
                             (self.getWidth() - textsurface.get_width() - 10, 3*( textsurface.get_height() + 2)))

        else:
            textsurface = self._scoreLabel.render("Press H for help", False, (255, 255, 255))
            self.screen.blit(textsurface, (self.getWidth()-textsurface.get_width()-10, 0))

    def guiUpdate(self):
        textsurface = self._scoreLabel.render('Score: ' + str(score), False, (255, 255, 255))
        self.screen.blit(textsurface, (10, 0))
        self.helpText()


    def update(self):
        self.screen.fill((0, 0, 0))
