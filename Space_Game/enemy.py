import pygame
import random

import utility

class Enemy(object):

    def __init__(self, screen):
        self._screen = screen
        # Enemy sprite
        self._sprite = pygame.image.load("sprites/enemy.png")
        # Enemy position
        self._position = self._screen.getCenter()
        # Enemy scale
        self._scale = pygame.math.Vector2(1, 1)

        self._speed = 1

    def setSpeed(self, speed):
        self._speed = speed

    def setPosition(self, position):
        self._position = position

    def scale(self, scale):
        width = int(self._sprite.get_width() * (scale.x / self._scale.x))
        height = int(self._sprite.get_height() * (scale.y / self._scale.y))
        self._sprite = pygame.transform.scale(self._sprite, (width, height))
        self._scale = scale

    def goRight(self):
        self._position.x += self._speed
        if self._position.x > self._screen.getWidth()-self._sprite.get_width():
            self._position.x = self._screen.getWidth() - self._sprite.get_width()

    def goLeft(self):
        self._position.x -= self._speed
        if self._position.x < 0:
            self._position.x = 0

    def goDown(self, speed):
        self._position.y += speed

    def update(self):
        self._screen.draw(self._sprite, self._position)
        #pygame.draw.rect(self._sprite,(0,0,100),(0,0,self._sprite.get_width(), self._sprite.get_height()))

    def getPosition(self):
        return self._position

    def getGunPosition(self):
        return pygame.math.Vector2(self._position.x + self._sprite.get_width()/2, self._position.y + self._sprite.get_height()+1)

    def getSize(self):
        return pygame.math.Vector2(self._sprite.get_width(), self._sprite.get_height())



class EnemyCluster(object):

    clusterSize = None
    clusterPosition = None


    def __init__(self, screen):
        self._screen = screen
        self._enemyList = []
        self._goingRight = True
        self._goingDown = False
        self._speed_x = 1
        self._speed_y = 1

        self.generateEnemyCluster()

    def generateEnemyCluster(self, rows = 5, again = False):
        newEnemy = Enemy(self._screen)
        enemySize = newEnemy.getSize()
        enemyNum =  2 * self._screen.getWidth() / (3 * enemySize.x)

        dist_x = enemySize.x * 1.5
        dist_y = enemySize.y * 1.5

        pos_x = 0
        pos_y = 0
        if again:
            pos_y = -1*(rows)*dist_y-1
            self._speed_x += 0.5

        for j in range(0, rows):

            for i in range(0, int(enemyNum) - 1):
                newEnemy = Enemy(self._screen)
                pos_x += dist_x
                newEnemy.setPosition(pygame.math.Vector2(pos_x, pos_y))
                newEnemy.setSpeed(self._speed_x)
                self._enemyList.append(newEnemy)

            pos_y += dist_y
            pos_x = 0

        self.clusterSize = pygame.math.Vector2(int(enemyNum-1)*dist_x, (rows-1)*dist_y)
        self.clusterPosition = pygame.math.Vector2(dist_x, dist_y)
        self._speed_y = dist_y/3

    def checkHits(self, laserList):
        enemySize = self._enemyList[0].getSize()
        laserPos = None
        for laser in laserList:
            laserPos = laser.getPosition()
            for enemy in self._enemyList:
                dist = laserPos.distance_to(enemy.getPosition() + enemySize/2)
                if dist < enemySize.x and dist < enemySize.y:
                    self._enemyList.remove(enemy)
                    utility.score += 2
                    laser.toKill = True
                    break

    def getRandomGun(self):
        enList = []
        pos = self._enemyList[len(self._enemyList)-1].getPosition()

        for enemy in self._enemyList:
            if enemy.getPosition().y == pos.y:
                enList.append(enemy)
        return enList[int(random.uniform(0, len(enList)))].getGunPosition()

    def update(self):
        if (self.clusterPosition.x < 0 and self._goingRight == False) or \
                (self.clusterPosition.x + self.clusterSize.x > self._screen.getWidth() and self._goingRight == True):
            self._goingRight = not self._goingRight
            self._goingDown = True


        for enemy in self._enemyList:
            if self._goingDown:
                enemy.goDown(self._speed_y)
            elif self._goingRight:
                enemy.goRight()
            else:
                enemy.goLeft()
            enemy.update()

        if self._goingDown:
            self.clusterPosition.y += self._speed_x
        elif self._goingRight:
            self.clusterPosition.x += self._speed_x
        else:
            self.clusterPosition.x -= self._speed_x

        self._goingDown = False

        if(len(self._enemyList)==0):
            self.generateEnemyCluster(5, True)


